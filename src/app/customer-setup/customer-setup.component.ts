import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AppService } from '../service/app.service';
import { API } from '../constants';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customer-setup',
  templateUrl: './customer-setup.component.html',
  styleUrls: ['./customer-setup.component.scss']
})
export class CustomerSetupComponent implements OnInit {
  registerForm: FormGroup;
  countryList: any;
  statesList: any;
  partnersData: any;
  activeCustomerType: string;
  clientType: string;
  // numberFormat = '\(?\d{3}\)?[. -]? *\d{3}[. -]? *[. -]?\d{4}';
  numberFormat = '^((\\+91-?)|0)?[0-9]{10}$';

  constructor(private fb: FormBuilder,
    private appService: AppService,
    private toastr: ToastrService) {
    //selectedCustomerId , customerType

    this.registerForm = this.fb.group({
      customerNm: ['', Validators.required],
      emailId: ['', [Validators.required, Validators.email]],
      website: ['', Validators.required],
      mobileNbr: ['', [Validators.required, Validators.pattern(this.numberFormat)]],
      landLineNbr: ['', Validators.required],
      addressLine1: ['', Validators.required],
      addressLine2: ['', Validators.required],
      addressLine3: ['', Validators.required],
      countryId: ['', Validators.required],
      stateId: ['', Validators.required],
      cityNm: ['', Validators.required],
      zipCd: ['', Validators.required],
      contactFirstNm: ['', Validators.required],
      contactMiddleNm: ['', Validators.required],
      contactLastNm: ['', Validators.required],
      contactEmailId: ['', [Validators.required, Validators.email]],
      contactMobileNbr: ['', Validators.required],
      contactLandLineNbr: ['', Validators.required],
      contactDesignation: ['', Validators.required],
      customerType: ['', Validators.required],
      selectedCustomerId: [''],
      clientType: ['MP'],
      reportingCustomerId: ['']
    });
  }

  ngOnInit(): void {
    this.appService.getRequest(API.COUNTRY_DATA).subscribe(res => {
      if (res.status == 200) {
        this.countryList = res.result;
      }
    }, err => {
      console.log(err);
    });
    this.onValueChanges();
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this.appService.postRequest(API.INSERT_CUSTOMERS_DATA, [this.registerForm.value]).subscribe(res => {
        if (res.status == 200) {
          this.toastr.success(res.result);
          this.registerForm.reset();
        }
      }, err => {
        console.log(err);
      })
    } else {
      this.toastr.error("Please enter all the required information")
    }
  }
  resetForm() {
    this.registerForm.reset();
  }

  onValueChanges() {
    this.registerForm.get('countryId').valueChanges.subscribe(val => {
      this.appService.getRequestWithParams(API.STATE_DATA_WITH_COUNTRY_ID, { countryLookupId: val }).subscribe(res => {
        if (res.status == 200) {
          this.statesList = res.result;
        }
      })
    });
    this.registerForm.get('customerType').valueChanges.subscribe(val => {
      this.activeCustomerType = val;
      const selectCustomer = this.registerForm.get('selectedCustomerId');
      if (val == 'FP') {
        this.getPartnersData(val);
        selectCustomer.setValidators(Validators.required);
      } else {
        selectCustomer.clearValidators();
      }
      selectCustomer.updateValueAndValidity();
    });

    this.registerForm.get('clientType').valueChanges.subscribe(val => {
      this.clientType = val;
      const reportCustomer = this.registerForm.get('reportingCustomerId');
      if (val !== 'MP') {
        this.getPartnersData(val);
        reportCustomer.setValidators(Validators.required);
      } else {
        reportCustomer.clearValidators();
      }
      reportCustomer.updateValueAndValidity();
    })
  }

  getPartnersData(val) {
    this.appService.getRequestWithParams(API.FETCH_PARTNER_DATA_BY_TYPE, { type: val }).subscribe(res => {
      if (res.status == 200) {
        this.partnersData = res.result;
      }
    })
  }


}
