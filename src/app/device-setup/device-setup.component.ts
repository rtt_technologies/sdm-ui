import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AppService } from '../service/app.service';
import { API } from '../constants';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AddDeviceComponent } from '../add-device/add-device.component';


@Component({
  selector: 'app-device-setup',
  templateUrl: './device-setup.component.html',
  styleUrls: ['./device-setup.component.scss']
})
export class DeviceSetupComponent implements OnInit {
  selectedClient: any;
  displayedColumns: string[] = ['sNo', 'device', 'dateOfInstall', 'installBy', 'sku', 'color', 'action'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private fb: FormBuilder,
    private appService: AppService,
    private dialog: MatDialog) {
  }

  ngOnInit(): void {
    const data = [
      { sNo: 1, device: 'Amazon Echo', dateOfInstall: '07/05/2020', installBy: 'John', sku: '34587952', color: 'Black' },
      { sNo: 2, device: 'Amazon Show', dateOfInstall: '24/02/2020', installBy: 'Dhen', sku: '345878952', color: 'white' },
    ]
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  addDevice() {
    const dialogRef = this.dialog.open(AddDeviceComponent, {
      // width: '380px',
      hasBackdrop: true,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
