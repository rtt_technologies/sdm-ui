export const API = {
    AUTHENTICATE: 'authenticate',
    COUNTRY_DATA: 'fetchallcountrydata',
    STATE_DATA_WITH_COUNTRY_ID: 'fetchstatebyCountryid',
    FETCH_PARTNER_DATA_BY_TYPE: 'fetchpartnerdatabytype',
    INSERT_CUSTOMERS_DATA: 'insertcustomerdata'
}