import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  cpForm: FormGroup;
  // validatePwd: boolean = false;

  constructor(public dialogRef: MatDialogRef<ChangePasswordComponent>,
    private _fb: FormBuilder) {

    this.cpForm = this._fb.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    }, {
      validators: this.validatePassword.bind(this)
    });
  }

  ngOnInit(): void {
  }

  validatePassword(formGroup: FormGroup) {

    // const pwd = this.cpForm.get('newPassword').value;
    // const cPwd = this.cpForm.get('confirmPassword').value;
    // // this.validatePwd = () ? false : true;
    // return pwd == cPwd ? null : { validatePwd: true }
  }

}
