import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerSetupComponent } from './customer-setup/customer-setup.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { AuthGuard } from './guard/auth.guard';
import { DeviceSetupComponent } from './device-setup/device-setup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';


const routes: Routes = [
  { path: '', component: SignInComponent },
  { path: 'customer-setup', component: CustomerSetupComponent, canActivate: [AuthGuard] },
  { path: 'device-setup', component: DeviceSetupComponent, canActivate: [AuthGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
