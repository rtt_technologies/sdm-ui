import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../service/app.service';
import { API } from '../constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  signIn: FormGroup;
  constructor(private _fb: FormBuilder,
    private appService: AppService,
    private router: Router) {

    this.signIn = this._fb.group({
      userName: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    if (localStorage.auth_details) {
      this.router.navigateByUrl('/customer-setup');
    }
  }

  login() {
    if (this.signIn.valid) {
      localStorage.clear();
      this.appService.postRequest(API.AUTHENTICATE, this.signIn.value).subscribe(res => {
        console.log(res);
        if (res.status == 200) {
          localStorage.auth_details = JSON.stringify(res.result);
          this.router.navigateByUrl('/customer-setup');
        }
      }, err => {
        console.log(err);
      })
    }
    //postRequest
  }
}
