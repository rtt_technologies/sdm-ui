import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../service/app.service';
import { API } from '../constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  forgotForm: FormGroup;
  constructor(private _fb: FormBuilder,
    private appService: AppService,
    private router: Router) {

    this.forgotForm = this._fb.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }


  ngOnInit(): void {
  }

  forgot() {

  }
}
