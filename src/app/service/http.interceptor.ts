import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const auth_details = JSON.parse(localStorage.getItem('auth_details'));

        if (auth_details) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + auth_details['jwtToken']) });
            // request = request.clone({ headers: request.headers.set('userId', auth_details['userId']) });
        }

        if (!request.headers.has('Content-Type')) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json; charset=utf-8') });
        }
        return next.handle(request).pipe(catchError((error: HttpErrorResponse) => {
            return throwError(error);
        }));
    }
}