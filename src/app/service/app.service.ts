import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class AppService {
    baseURL = environment.BASE_PATH_URL;
    constructor(private http: HttpClient) { }

    getRequest(api) {
        const apiUrl = this.baseURL + api;
        return this.http.get<any>(apiUrl);
    }
    getRequestWithParams(api, params) {
        const apiUrl = this.baseURL + api;
        
        return this.http.get<any>(apiUrl, { params });
    }
    postRequest(api, data) {
        const apiUrl = this.baseURL + api;
        return this.http.post<any>(apiUrl, data);
    }
    putRequest(api, data) {
        const apiUrl = this.baseURL + api;
        return this.http.put<any>(apiUrl, data);
    }
    deleteRequest(api, data) {
        const apiUrl = this.baseURL + api;
        return this.http.delete<any>(apiUrl, data);
    }
}